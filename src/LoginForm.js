
function LoginForm() {

  function sendLoginRequest() {
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    if (username.length > 0 && password.length > 0) {
      const axios = require('axios');

      const params = new URLSearchParams();
      params.append('client_id', 'Homework4.ConsoleApp.ClientId');
      params.append('client_secret', '4F9C8F34-74C9-4734-B1EA-B01C7C539520');
      params.append('grant_type', 'client_credentials');
      params.append('username', username);
      params.append('password', password);

      const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }

      axios.post('/connect/token', params, config)
        .then((result) => {
          document.getElementById("jwt").value = result.data.access_token;
        })
        .catch((error) => {
          document.getElementById("jwt").value = error;
        })

    }
  }

  return (
    <div>
      <div>
        <label htmlFor="username">Username:&nbsp;</label>
        <input type="text" id="username" />
      </div>
      <div>
        <label htmlFor="password">Password:&nbsp;</label>
        <input type="password" id="password" />
      </div>
      <div>
        <input type="submit" value="Login" onClick={sendLoginRequest} />
      </div>
      <br />
      <br />
      <br />
      <div>
        <label htmlFor="jwt">JWT:&nbsp;</label>
        <br />
        <textarea id="jwt" disabled rows="10" cols="50"></textarea>
      </div>
    </div>
  );
}

export default LoginForm;
